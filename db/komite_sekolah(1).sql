-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 21, 2017 at 10:57 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `komite_sekolah`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `tgl_lahir` varchar(12) NOT NULL,
  `jk` varchar(25) NOT NULL,
  `alamat` text NOT NULL,
  `telepon` varchar(15) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `nama`, `tgl_lahir`, `jk`, `alamat`, `telepon`, `foto`, `email`, `password`, `status`) VALUES
(1, 'astriyanti', '07/13/1995', 'Perempuan', 'jalan abunawas no.099', '08114052207', 'IMG_0366.JPG', 'astriyanti@gmail.com', '52e7e539c9b93fc51c0fb618cae93d8b', 'admin'),
(2, 'trtyet', '2017-07-10', 'Laki - Laki', 'yrjyfj', '78687698', 'IMG_0396.JPG', 'rhmt@gmail.com', '44f437ced647ec3f40fa0841041871cd', 'admin'),
(3, 'SMA 1 SAMPARA', '2017-07-05', 'Perempuan', 'SAMPARA', '081123984789', '20170111_224455.jpg', 'SMA1SAMPARA@gmail.com', '42cc170c9185e6a4f99bf24c9379480d', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id_pembayaran` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `jenis_pembayaran` varchar(20) NOT NULL,
  `semester` varchar(11) NOT NULL,
  `jml_pembayaran` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`id_pembayaran`, `id_siswa`, `jenis_pembayaran`, `semester`, `jml_pembayaran`, `tanggal`, `time`) VALUES
(15, 15, 'KOMITE', 'semester 1', 800000, '2017-07-16', '02:31:59'),
(16, 15, 'KOMITE', 'semester 2', 800000, '2017-07-16', '03:06:08'),
(17, 15, 'KOMITE', 'semester 3', 800000, '2017-07-16', '03:06:57'),
(18, 15, 'KOMITE', 'semester 4', 800000, '2017-07-16', '03:08:04'),
(23, 15, 'KOMITE', 'semester 6', 800000, '2017-07-17', '03:39:25'),
(24, 15, 'KOMITE', 'semester 5', 800000, '2017-07-18', '09:21:27'),
(32, 20, 'KOMITE', 'semester 1', 800000, '2017-07-20', '02:06:26'),
(33, 20, 'KOMITE', 'semester 2', 800000, '2017-07-20', '02:06:45'),
(34, 21, 'KOMITE', 'semester 1', 105000, '2017-07-21', '09:09:15'),
(35, 21, 'KOMITE', 'semester 2', 100000, '2017-07-21', '09:11:49'),
(36, 22, 'KOMITE', 'semester 3', 210000, '2017-07-21', '03:25:11'),
(37, 22, 'KOMITE', 'semester 4', 105000, '2017-07-21', '03:25:30');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id_siswa` int(11) NOT NULL,
  `nisn` char(10) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `kelas` varchar(10) NOT NULL,
  `jenis_kelamin` varchar(25) NOT NULL,
  `tmpt_lahir` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `agama` varchar(25) NOT NULL,
  `alamat` text NOT NULL,
  `rt_rw` varchar(6) NOT NULL,
  `kelurahan` varchar(100) NOT NULL,
  `kecamatan` varchar(100) NOT NULL,
  `kode_pos` int(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telepon` varchar(15) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `nama_ayah` varchar(100) NOT NULL,
  `pekerjaan_ayah` varchar(50) NOT NULL,
  `penghasilan_ayah` varchar(30) NOT NULL,
  `nomor_hp_ayah` varchar(15) NOT NULL,
  `nama_ibu` varchar(100) NOT NULL,
  `pekerjaan_ibu` varchar(50) NOT NULL,
  `penghasilan_ibu` varchar(30) NOT NULL,
  `nomor_hp_ibu` varchar(15) NOT NULL,
  `status` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id_siswa`, `nisn`, `password`, `nama`, `kelas`, `jenis_kelamin`, `tmpt_lahir`, `tgl_lahir`, `agama`, `alamat`, `rt_rw`, `kelurahan`, `kecamatan`, `kode_pos`, `email`, `telepon`, `foto`, `nama_ayah`, `pekerjaan_ayah`, `penghasilan_ayah`, `nomor_hp_ayah`, `nama_ibu`, `pekerjaan_ibu`, `penghasilan_ibu`, `nomor_hp_ibu`, `status`) VALUES
(15, '8927353829', '039a29fbddb83a0dfa1cb50a1d93fa86', 'bawo ganteng', 'XII', 'Laki - Laki', 'amonggedo', '1995-12-14', 'hindu', 'jl.abunawas no.08', '002/00', 'bende', 'kadia', 93117, 'bawo@gmail.com', '08114052999', 'IMG_0432.JPG', 'amin', 'Rp 500.000 - Rp 1.000.000', 'PNS', '0823241512', 'mariadb', 'PNS', 'Rp 2.000.000 - Rp 3.000.000', '081725243721', 'user'),
(20, '3489237594', 'f2b954b040a62c2a7c7e5e7a998ae154', 'nia', 'X', 'Perempuan', 'rawua', '2017-07-02', 'islam', 'rawua', '002/00', 'rawua', 'sampara', 55874, 'nia@gmail.com', '97969', '20157851_1938135256459268_9046238395456654134_o.jpg', 'hviusd', 'NELAYAN', 'Rp 2.000.000 - Rp 3.000.000', '88588', 'nkdhaa', 'IBU RUMAH TANGGA', 'Rp 500.000 - Rp 1.000.000', '5897596', 'user'),
(21, '6092875034', '02ea2ae2a237c042285e093e6972eaa9', 'nani', 'X', 'Perempuan', 'rawua', '2017-07-02', 'islam', 'rawua', '002/00', 'sampara', 'sampara', 38925, 'nani@gmail.com', '0912895688', '20170111_224442.jpg', 'jdaw', 'Rp 500.000 - Rp 1.000.000', 'WIRASWASTA', '879134615', 'dksfhoesi', 'IBU RUMAH TANGGA', 'Rp 500.000 - Rp 1.000.000', '463277897', 'user'),
(22, '0011002200', '20c1a26a55039b30866c9d0aa51953ca', 'astri', 'XI', 'Perempuan', 'kendari', '1998-09-03', 'islam', 'kendari', '002/00', 'mandonga', 'mandonga', 98308, 'astry@gmail', '081934470883', 'PhotoGrid_1482218028836-1_mh1493306728817.jpg', 'ayah', 'WIRASWASTA', 'Rp 500.000 - Rp 1.000.000', '094028492', 'ibu', 'IBU RUMAH TANGGA', '- Pilih Penghasilan Ibu -', '6584838', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `id_pembayaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
