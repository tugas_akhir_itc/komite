<?php 
	include"../inc/config.php";
	include"../inc/function.php"; 
	validate_admin_not_login("login.php");
	include"layout/header.php";
?>
  <section class="content-header">
      <h1>
        Detail Pembayaran Siswa
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="pembayaran.php">Pembayaran Siswa</a></li>
        <li class="active">Detail Pembayaran Siswa</li>
      </ol>
    </section>
   <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
			<h3 class="box-title"></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
		
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
		  
		  
			<div class="col-md-12">
			 
			 <table id="example" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>No</th>
								<th>Nisn</th>
								<th>Nama</th>
								<th>Jenis Pembayaran</th>
								<th>Semester</th>
								<th>Jumlah Pembayaran</th>
								<th>Waktu Pembayaran</th>
								<th>Aksi</th>
								
							</tr>
						</thead>
						<tbody>
							<?php
								$p = mysql_fetch_object(mysql_query("select * from siswa where id_siswa=$_GET[id]"));
								$q = mysql_query("select * from siswa inner join pembayaran on siswa.id_siswa=pembayaran.id_siswa where nisn='$p->nisn' order by semester asc ") or die (mysql_error());
								while ($data = mysql_fetch_object($q)) {
									?>
									
									<tr>
										<td><?php echo $no++; ?></td>
										<td><?php echo $data->nisn; ?></td>
										<td><?php echo $data->nama; ?></td>
										<td><?php echo $data->jenis_pembayaran; ?></td>
										<td><?php echo $data->semester; ?></td>
										<td><?php echo 'Rp.' . number_format($data->jml_pembayaran, 2,',','.') ?></td>
										<td><?php echo $data->tanggal; ?> / <?php echo $data->time; ?></td>
										<td><a class='btn btn-warning btn-xs' title='Edit Pembayaran Siswa' href='edit_pembayaran.php?id=<?php echo $data->id_pembayaran; ?>'><span class='glyphicon glyphicon-edit'></span></a></td>
										
									</tr>
									<?php
								}
							?>
							
						</tbody>
					</table>
					<button type="reset" onclick="history.back(-1)" class="btn btn-default">Back</button>
        </div>
        <!-- /.box-body -->
       
      </div>
      <!-- /.box -->

      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <?php include"layout/footer.php"; ?>