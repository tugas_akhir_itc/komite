<?php 
	include"../inc/config.php";
	include"../inc/function.php"; 
	validate_admin_not_login("login.php");
	include"layout/header.php";
	
	$query = "select * from siswa where id_siswa = '$_GET[id]'";
	$result = mysql_query($query);
	$data = mysql_fetch_object($result);
?>
  <section class="content-header">
      <h1>
        Edit Data Siswa
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="data_siswa.php">Data Siswa</a></li>
        <li class="active">Edit Data Siswa</li>
      </ol>
    </section>
   <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
			<h3 class="box-title"></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" onclick="history.back(-1)" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
		
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
		  
		  <?php 
			if(!empty($_POST)){
			$foto=$_FILES['foto']['name'];
			extract($_POST);
			$pass = md5($pass);
			$q = mysql_query("update siswa SET nisn='$nisn', password='$pass', nama='$nm_siswa', kelas='$kelas', jenis_kelamin='$jk', tmpt_lahir='$tmpt_lhr', tgl_lahir='$tgl_lhr', agama='$agama', alamat='$alamat', rt_rw='$rt_rw', kelurahan='$kel', kecamatan='$kec', kode_pos='$kd_pos', email='$email', telepon='$no_hp', foto='$foto', nama_ayah='$nm_ayah', pekerjaan_ayah='$pk_ayah', penghasilan_ayah='$hasil_ayah', nomor_hp_ayah='$nmr_ayah', nama_ibu='$nm_ibu', pekerjaan_ibu='$pk_ibu', penghasilan_ibu='$hasil_ibu', nomor_hp_ibu='$nmr_ibu', status='user' where id_siswa='$_GET[id]'");
			move_uploaded_file($_FILES['foto']['tmp_name'], "../image/".$foto);
				if($q){  
				
				echo "<script>alert('Data Berhasil Disimpan');document.location.href='data_siswa.php'</script>\n";
					} else {
					echo "<script>alert('Terjadi kesalahan dalam pengisian form. Silahkan Coba Lagi');document.location.href='data_siswa.php'</script>\n";
						}	
					}
				?>
		  
			<div class="col-md-6">
		 	 <form class="form" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label>NISN</label>
                <input type="text" class="form-control" name="nisn" value="<?php echo $data->nisn?>" required>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>PASSWORD</label>
                <input type="password" class="form-control" name="pass" placeholder="********" required>
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                <label>KELAS</label>
                <select class="form-control select2" name="kelas" style="width: 100%;" required>
					<option value="X" <?php if($data->kelas == 'X'){ echo 'selected'; } ?> >X</option>
					<option value="XI" <?php if($data->kelas == 'XI'){ echo 'selected'; } ?> >XI</option>
					<option value="XII" <?php if($data->kelas == 'XII'){ echo 'selected'; } ?> >XII</option>
				</select>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>TANGGAL LAHIR</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" name="tgl_lhr" id="datepicker" value="<?php echo $data->tgl_lahir ?>" required>
                </div>
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
			<div class="col-md-6">
              <div class="form-group">
                <label>NAMA SISWA</label>
                <input type="text" class="form-control" name="nm_siswa" value="<?php echo $data->nama ?>" required>
              </div>
              <div class="form-group">
                <label>ALAMAT SISWA</label>
                <input type="text" class="form-control" name="alamat" value="<?php echo $data->alamat ?>" required>
              </div>
            </div>
			
			<div class="col-md-6">
              <div class="form-group">
                <label>JENIS KELAMIN</label>
				<select class="form-control select2" name="jk" style="width: 100%;" required>
				<?php
						$jenis_kelamin=$data->jenis_kelamin;
						if($jenis_kelamin=='Laki - Laki'){
						?>
							<option value="Laki - Laki" selected="selected">Laki - Laki</option>
                			<option value="Perempuan" >Perempuan</option>
						<?php
						}
						else{
						?>
							<option value="Laki - Laki">Laki - Laki</option>
                			<option value="Perempuan" selected="selected">Perempuan</option>
								
						<?php
						}
				?>
                </select>
              </div>
              <div class="form-group">
                <label>NAMA AYAH</label>
                <input type="text" class="form-control" name="nm_ayah" value="<?php echo $data->nama_ayah ?>" required>
              </div>
            </div>
			
			<div class="col-md-6">
              <div class="form-group">
                <label>RT/RW</label>
                <input type="text" class="form-control" name="rt_rw" placeholder="001/001" value="<?php echo $data->rt_rw ?>">
              </div>
              <div class="form-group">
                <label>KECAMATAN</label>
                <input type="text" class="form-control" name="kec" value="<?php echo $data->kecamatan ?>">
              </div>
            </div>
			
			<div class="col-md-6">
              <div class="form-group">
                <label>PEKERJAAN AYAH</label>
				<select class="form-control select2" name="hasil_ayah" style="width: 100%;" required>
					<option value="PNS" <?php if($data->pekerjaan_ayah == 'PNS'){ echo 'selected'; } ?> >PNS</option>
					<option value="POLRI/TNI" <?php if($data->pekerjaan_ayah == 'POLRI/TNI'){ echo 'selected'; } ?> >POLRI/TNI</option>
					<option value="WIRASWASTA" <?php if($data->pekerjaan_ayah == 'WIRASWASTA'){ echo 'selected'; } ?> >WIRASWASTA</option>
					<option value="PETANI" <?php if($data->pekerjaan_ayah == 'PETANI'){ echo 'selected'; } ?> >PETANI</option>
					<option value="NELAYAN" <?php if($data->pekerjaan_ayah == 'NELAYAN'){ echo 'selected'; } ?> >NELAYAN</option>
					<option value="DLL" <?php if($data->pekerjaan_ayah == 'DLL'){ echo 'selected'; } ?> >DLL</option>
				</select>
              </div>
              <div class="form-group">
                <label>PENGHASILAN AYAH</label>
				<select class="form-control select2" name="pk_ayah" style="width: 100%;" required>
					<option value="Rp 500.000 - Rp 1.000.000" <?php if($data->penghasilan_ayah == 'Rp 500.000 - Rp 1.000.000'){ echo 'selected'; } ?> >Rp 500.000 - Rp 1.000.000</option>
					<option value="Rp 1.00.000 - Rp 2.000.000" <?php if($data->penghasilan_ayah == 'Rp 1.00.000 - Rp 2.000.000'){ echo 'selected'; } ?> >Rp 1.00.000 - Rp 2.000.000</option>
					<option value="Rp 2.000.000 - Rp 3.000.000" <?php if($data->penghasilan_ayah == 'Rp 2.000.000 - Rp 3.000.000'){ echo 'selected'; } ?> >Rp 2.000.000 - Rp 3.000.000</option>
					<option value="Rp 3.000.000 - Rp 4.000.000" <?php if($data->penghasilan_ayah == 'Rp 3.000.000 - Rp 4.000.000'){ echo 'selected'; } ?> >Rp 3.000.000 - Rp 4.000.000</option>
					<option value="> Rp 5.000.000" <?php if($data->penghasilan_ayah == '> Rp 5.000.000'){ echo 'selected'; } ?> >> Rp 5.000.000</option>
				</select>
              </div>
            </div>
			
			<div class="col-md-6">
              <div class="form-group">
                <label>KELURAHAN</label>
                <input type="text" class="form-control" name="kel" value="<?php echo $data->kelurahan ?>">
              </div>
              <div class="form-group">
                <label>KODE POS</label>
                <input type="text" class="form-control" name="kd_pos" value="<?php echo $data->kode_pos ?>">
              </div>
            </div>
			
			<div class="col-md-6">
              <div class="form-group">
                <label>NOMOR HP AYAH</label>
                <input type="text" class="form-control" name="nmr_ayah" value="<?php echo $data->nomor_hp_ayah ?>" required>
              </div>
              <div class="form-group">
                <label>NAMA IBU</label>
                <input type="text" class="form-control" name="nm_ibu" value="<?php echo $data->nama_ibu ?>" required>
              </div>
            </div>
			
			<div class="col-md-6">
              <div class="form-group">
                <label>NO HANDPHONE</label>
                <input type="text" class="form-control" name="no_hp" value="<?php echo $data->telepon ?>" required>
              </div>
              <div class="form-group">
                <label>AGAMA</label>
                <select class="form-control select2" name="agama" style="width: 100%;" required>
					<option value="islam" <?php if($data->agama == 'islam'){ echo 'selected'; } ?> >Islam</option>
					<option value="kristen" <?php if($data->agama == 'kristen'){ echo 'selected'; } ?> >Kristen</option>
					<option value="hindu" <?php if($data->agama == 'hindu'){ echo 'selected'; } ?> >Hindu</option>
					<option value="budha" <?php if($data->agama == 'budha'){ echo 'selected'; } ?> >Budha</option>
					<option value="konghuchu" <?php if($data->agama == 'konghucu'){ echo 'selected'; } ?> >Konghucu</option>
				</select>
              </div>
            </div>
			
			<div class="col-md-6">
              <div class="form-group">
			  <label>PEKERJAAN IBU</label>
				<select class="form-control select2" name="pk_ibu" style="width: 100%;" required>
					<option value="PNS" <?php if($data->pekerjaan_ibu == 'PNS'){ echo 'selected'; } ?> >PNS</option>
					<option value="POLRI/TNI" <?php if($data->pekerjaan_ibu == 'POLRI/TNI'){ echo 'selected'; } ?> >POLRI/TNI</option>
					<option value="WIRASWASTA" <?php if($data->pekerjaan_ibu == 'WIRASWASTA'){ echo 'selected'; } ?> >WIRASWASTA</option>
					<option value="PETANI" <?php if($data->pekerjaan_ibu == 'PETANI'){ echo 'selected'; } ?> >PETANI</option>
					<option value="NELAYAN" <?php if($data->pekerjaan_ibu == 'NELAYAN'){ echo 'selected'; } ?> >NELAYAN</option>
					<option value="IBU RUMAH TANGGA" <?php if($data->pekerjaan_ibu == 'IBU RUMAH TANGGA'){ echo 'selected'; } ?> >IBU RUMAH TANGGA</option>
					<option value="DLL" <?php if($data->pekerjaan_ibu == 'DLL'){ echo 'selected'; } ?> >DLL</option>
				</select>

              </div>
              <div class="form-group">
			  <label>PENGHASILAN IBU</label>
				<select class="form-control select2" name="hasil_ibu" style="width: 100%;" required>
					<option value="Rp 500.000 - Rp 1.000.000" <?php if($data->penghasilan_ibu == 'Rp 500.000 - Rp 1.000.000'){ echo 'selected'; } ?> >Rp 500.000 - Rp 1.000.000</option>
					<option value="Rp 1.00.000 - Rp 2.000.000" <?php if($data->penghasilan_ibu == 'Rp 1.00.000 - Rp 2.000.000'){ echo 'selected'; } ?> >Rp 1.00.000 - Rp 2.000.000</option>
					<option value="Rp 2.000.000 - Rp 3.000.000" <?php if($data->penghasilan_ibu == 'Rp 2.000.000 - Rp 3.000.000'){ echo 'selected'; } ?> >Rp 2.000.000 - Rp 3.000.000</option>
					<option value="Rp 3.000.000 - Rp 4.000.000" <?php if($data->penghasilan_ibu == 'Rp 3.000.000 - Rp 4.000.000'){ echo 'selected'; } ?> >Rp 3.000.000 - Rp 4.000.000</option>
					<option value="> Rp 5.000.000" <?php if($data->penghasilan_ibu == '> Rp 5.000.000'){ echo 'selected'; } ?> >> Rp 5.000.000</option>
				</select>
               </div>
            </div>
			
			<div class="col-md-6">
			<div class="form-group">
                <label>EMAIL</label>
                <input type="email" class="form-control" name="email" value="<?php echo $data->email ?>" required>
              </div>
             <div class="form-group">
                <label>TEMPAT LAHIR</label>
                <input type="text" class="form-control" name="tmpt_lhr" value="<?php echo $data->tmpt_lahir?>" required>
              </div>
            </div>
			
			<div class="col-md-6">
              <div class="form-group">
                <label>NOMOR HP IBU</label>
                <input type="text" class="form-control" name="nmr_ibu" value="<?php echo $data->nomor_hp_ibu ?>" required>
              </div>
			  <div class="form-group">
                <label>FOTO</label>
                <input type="file" id="exampleInputFile" name="foto" required>
              </div>
            </div>
          </div>
		  
		  <BR>
          <div class="box-footer">
                <button type="reset" onclick="history.back(-1)" class="btn btn-default">Back</button>
                <button type="submit" name="form-edit" class="btn btn-info pull-right">Submit</button>
          </div>
		 </form>
        </div>
        <!-- /.box-body -->
       
      </div>
      <!-- /.box -->

      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <?php include"layout/footer.php"; ?>