<?php 
	include"../inc/config.php";
	include"../inc/function.php"; 
	validate_admin_not_login("login.php");
	include"layout/header.php";
	
	$p = mysql_fetch_object(mysql_query("select * from siswa where id_siswa=$_GET[id]"));
	$q = mysql_query("select * from siswa inner join pembayaran on siswa.id_siswa=pembayaran.id_siswa where nisn='$p->nisn'") or die (mysql_error());
	$data = mysql_fetch_object($q)
?>
  <section class="content-header">
      <h1>
        Pembayaran Siswa
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Data Siswa</a></li>
        <li class="active">Pembayaran Siswa</li>
      </ol>
    </section>
   <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
			<h3 class="box-title"></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
		
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
		  
		
				
				<?php 
					if(!empty($_POST)){
					extract($_POST); 
					$date=date('Y-m-d');
					$time=date('h:i:s');
					$x = mysql_num_rows(mysql_query("select * from siswa inner join pembayaran on siswa.id_siswa=pembayaran.id_siswa where nisn='$p->nisn' and semester='$_POST[semester]'"));
					if($x > 0){
						echo "<script>alert('Pembayaran Semester Ini Sudah Dibayar Sebelumnya');</script>\n"; 
					}else{
						mysql_query("insert into pembayaran Values(NULL,'$p->id_siswa', '$jns', '$semester', '$jml', '$date', '$time')");
						echo "<script>alert('Succes');document.location.href='detail_pembayaran.php?id=$p->id_siswa'</script>\n";
						}
					}
			
		    	?>	
				
			<div class="col-md-6">
			
		 	 <form class="form" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label>NISN</label>
                <input type="text" class="form-control" value="<?php echo $p->nisn?>" readonly="readonly">
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>NAMA</label>
                <input type="text" class="form-control" value="<?php echo $p->nama?>" readonly="readonly">
              </div>
			  <div class="form-group">
                <label>JENIS PEMBAYARAN</label>
                <input type="text" class="form-control" name="jns" value="KOMITE" readonly="readonly">
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
            <div class="col-md-6">
			<div class="form-group">
                <label>Kelas</label>
                <input type="text" class="form-control" value="<?php echo $p->kelas?>" readonly="readonly">
              </div>
              <div class="form-group">
                <label>SEMESTER</label>
                <select class="form-control select2" name="semester" style="width: 100%;" required>
                  <option selected="selected">- Pilih Semester -</option>
				  <option <?php echo ($p->kelas != 'X')? 'disabled' : '' ?> value="semester 1">Semester 1</option>
                  <option <?php echo ($p->kelas != 'X')? 'disabled' : '' ?> value="semester 2">Semester 2</option>
				  <option <?php echo ($p->kelas != 'XI')? 'disabled' : '' ?> value="semester 3">Semester 3</option>
				  <option <?php echo ($p->kelas != 'XI')? 'disabled' : '' ?> value="semester 4">Semester 4</option>
				  <option <?php echo ($p->kelas != 'XII')? 'disabled' : '' ?> value="semester 5">Semester 5</option>
				  <option <?php echo ($p->kelas != 'XII')? 'disabled' : '' ?> value="semester 6">Semester 6</option>
                </select>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>JUMLAH PEMBAYARAN</label>
                <input type="text" class="form-control" name="jml" required>
              </div>
              <!-- /.form-group -->
            </div>
			
          </div>
		  
		  <BR>
          <div class="box-footer">
                <button type="submit" name="form-input" class="btn btn-info pull-right">Submit</button>
				<button type="reset" onclick="history.back(-1)" class="btn btn-default pull-left">Back</button>
          </div>
		 </form>
        </div>
        <!-- /.box-body -->
       
      </div>
      <!-- /.box -->

      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <?php include"layout/footer.php"; ?>