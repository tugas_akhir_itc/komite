<?php 
	include"../inc/config.php";
	include"../inc/function.php"; 
	validate_admin_not_login("login.php");
	include"layout/header.php";
	
	$query = "select * from admin where id = '$_GET[id]'";
	$result = mysql_query($query);
	$data = mysql_fetch_object($result);
?>
  <section class="content-header">
      <h1>
        Detail Data Admin
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="data_siswa.php">Data Admin</a></li>
        <li class="active">Detail Admin</li>
      </ol>
    </section>
   <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
			<h3 class="box-title"></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" onclick="history.back(-1)" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
		
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
		  
		  	<img class='img-thumbnail' src="<?php echo '../image/'.$data->foto ?>"style='width:180px; height:200px' >
		  
			<div class="col-md-9">
			<table class="table table-bordered table-hover">
			 <tr>
                <td width="100px">EMAIL</td>
                <td width="200px"><?php echo $data->email?></td>
				<td width="100px">ALAMAT</td>
                <td width="200px"><?php echo $data->alamat?></td>
			</tr>
			<tr>
                <td>PASSWORD</td>
                <td>********</td>
				<td>TANGGAL LAHIR</td>
                <td><?php echo $data->tgl_lahir ?></td>
            </tr>
			<tr>
                <td>NAMA</td>
                <td><?php echo $data->nama ?></td>
				<td>JENIS KELAMIN</td>
                <td><?php echo $data->jk ?></td>
            </tr>
			<tr>
                <td>TELEPON</td>
                <td><?php echo $data->telepon ?></td>
            </tr>
			<tr>
                <td><button type="reset" onclick="history.back(-1)" class="btn btn-default">Back</button></td>
            </tr>
		 </table>
        </div>
        <!-- /.box-body -->
       
      </div>
      <!-- /.box -->

      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <?php include"layout/footer.php"; ?>