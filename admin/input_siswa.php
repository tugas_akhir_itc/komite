<?php 
	include"../inc/config.php";
	include"../inc/function.php"; 
	validate_admin_not_login("login.php");
	include"layout/header.php";
?>
  <section class="content-header">
      <h1>
        Tambah Data Siswa
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="data_siswa.php">Data Siswa</a></li>
        <li class="active">Tambah Data Siswa</li>
      </ol>
    </section>
   <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
			<h3 class="box-title"></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" onclick="history.back(-1)" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
		
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
		  
		  		<?php 
					if(!empty($_POST)){
					$foto=$_FILES['foto']['name'];
					extract($_POST); 
				
					$pass = md5($pass);
					$q = mysql_num_rows(mysql_query("select * from siswa where nisn ='$_POST[nisn]'"));
					if($q > 0){
						echo "<script>alert('Nisn sudah digunakan');</script>\n"; 
					}else{
						mysql_query("insert into siswa Values(NULL,'$nisn','$pass','$nm_siswa','$kelas','$jk','$tmpt_lhr','$tgl_lhr','$agama','$alamat','$rt_rw','$kel','$kec','$kd_pos','$email','$no_hp','$foto','$nm_ayah','$pk_ayah','$hasil_ayah','$nmr_ayah','$nm_ibu','$pk_ibu','$hasil_ibu','$nmr_ibu','user')");
						move_uploaded_file($_FILES['foto']['tmp_name'], "../image/".$foto);
						echo "<script>alert('Succes');document.location.href='data_siswa.php'</script>\n";
						}
					}
			
		    	?>
			
			<div class="col-md-6">
		 	 <form class="form" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label>NISN</label>
                <input type="text" class="form-control" name="nisn" maxlength="10" minlength="10" required>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>PASSWORD</label>
                <input type="password" class="form-control" name="pass" required>
              </div>
            </div>
			
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                <label>KELAS</label>
                <select class="form-control select2" name="kelas" style="width: 100%;" required>
				<option selected="selected">- Pilih Kelas -</option>
				  <option value="X">X</option>
                  <option value="XI">XI</option>
				  <option value="XII">XII</option>
                </select>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>TANGGAL LAHIR</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" name="tgl_lhr" id="datepicker" required>
                </div>
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
			<div class="col-md-6">
              <div class="form-group">
                <label>NAMA SISWA</label>
                <input type="text" class="form-control" name="nm_siswa" required>
              </div>
              <div class="form-group">
                <label>ALAMAT SISWA</label>
                <input type="text" class="form-control" name="alamat" required>
              </div>
            </div>
			
			<div class="col-md-6">
              <div class="form-group">
                <label>JENIS KELAMIN</label>
                <select class="form-control select2" name="jk" style="width: 100%;" required>
                  <option selected="selected">- Pilih Jenis Kelamin -</option>
				  <option value="Laki - Laki">Laki - Laki</option>
                  <option value="Perempuan">Perempuan</option>
                </select>
              </div>
              <div class="form-group">
                <label>NAMA AYAH</label>
                <input type="text" class="form-control" name="nm_ayah" required>
              </div>
            </div>
			
			<div class="col-md-6">
              <div class="form-group">
                <label>RT/RW</label>
                <input type="text" class="form-control" name="rt_rw" placeholder="001/001">
              </div>
              <div class="form-group">
                <label>KECAMATAN</label>
                <input type="text" class="form-control" name="kec">
              </div>
            </div>
			
			<div class="col-md-6">
              <div class="form-group">
                <label>PEKERJAAN AYAH</label>
				<select class="form-control select2" name="pk_ayah" style="width: 100%;" required>
				<option selected="selected">- Pilih Pekerjaan Ayah -</option>
				  <option value="PNS">PNS</option>
                  <option value="POLRI/TNI">POLRI/TNI</option>
				  <option value="WIRASWASTA">WIRASWASTA</option>
                  <option value="PETANI">PETANI</option>
				  <option value="NELAYAN">NELAYAN</option>
				  <option value="DLL.">DLL.</option>
                </select>
				
              </div>
              <div class="form-group">
                <label>PENGHASILAN AYAH</label>
				<select class="form-control select2" name="hasil_ayah" style="width: 100%;" required>
					<option selected="selected">- Pilih Penghasilan Ayah -</option>
				  <option value="Rp 500.000 - Rp 1.000.000">Rp 500.000 - Rp 1.000.000</option>
                  <option value="Rp 1.00.000 - Rp 2.000.000">Rp 1.00.000 - Rp 2.000.000</option>
				  <option value="Rp 2.000.000 - Rp 3.000.000">Rp 2.000.000 - Rp 3.000.000</option>
                  <option value="Rp 3.000.000 - Rp 4.000.000">Rp 3.000.000 - Rp 4.000.000</option>
				  <option value="> Rp 5.000.000">> Rp 5.000.000</option>
                </select>
              </div>
            </div>
			
			<div class="col-md-6">
              <div class="form-group">
                <label>KELURAHAN</label>
                <input type="text" class="form-control" name="kel">
              </div>
              <div class="form-group">
                <label>KODE POS</label>
                <input type="text" class="form-control" name="kd_pos">
              </div>
            </div>
			
			<div class="col-md-6">
              <div class="form-group">
                <label>NOMOR HP AYAH</label>
                <input type="text" class="form-control" name="nmr_ayah" required>
              </div>
              <div class="form-group">
                <label>NAMA IBU</label>
                <input type="text" class="form-control" name="nm_ibu" required>
              </div>
            </div>
			
			<div class="col-md-6">
              <div class="form-group">
                <label>NO HANDPHONE</label>
                <input type="text" class="form-control" name="no_hp" required>
              </div>
              <div class="form-group">
                <label>AGAMA</label>
                <select class="form-control select2" name="agama" style="width: 100%;" required>
                  <option selected="selected">- Pilih Agama -</option>
				  <option value="islam">Islam</option>
                  <option value="kristen">Kristen</option>
				  <option value="hindu">Hindu</option>
                  <option value="budha">Budha</option>
				  <option value="konghucu">Konghucu</option>
                </select>
              </div>
            </div>
			
			<div class="col-md-6">
              <div class="form-group">
                
				<label>PEKERJAAN IBU</label>
				<select class="form-control select2" name="pk_ibu" style="width: 100%;" required>
				<option selected="selected">- Pilih Pekerjaan Ibu -</option>
				  <option value="PNS">PNS</option>
                  <option value="POLRI/TNI">POLRI/TNI</option>
				  <option value="WIRASWASTA">WIRASWASTA</option>
                  <option value="PETANI">PETANI</option>
				  <option value="NELAYAN">NELAYAN</option>
				  <option value="IBU RUMAH TANGGA">IBU RUMAH TANGGA</option>
				  <option value="DLL.">DLL.</option>
                </select>
              </div>
              <div class="form-group">
                <label>PENGHASILAN IBU</label>
                <select class="form-control select2" name="hasil_ibu" style="width: 100%;" required>
				<option selected="selected">- Pilih Penghasilan Ibu -</option>
				  <option value="Rp 500.000 - Rp 1.000.000">Rp 500.000 - Rp 1.000.000</option>
                  <option value="Rp 1.00.000 - Rp 2.000.000">Rp 1.00.000 - Rp 2.000.000</option>
				  <option value="Rp 2.000.000 - Rp 3.000.000">Rp 2.000.000 - Rp 3.000.000</option>
                  <option value="Rp 3.000.000 - Rp 4.000.000">Rp 3.000.000 - Rp 4.000.000</option>
				  <option value="> Rp 5.000.000">> Rp 5.000.000</option>
                </select>
              </div>
            </div>
			
			<div class="col-md-6">
			<div class="form-group">
                <label>EMAIL</label>
                <input type="email" class="form-control" name="email" required>
              </div>
              
			  <div class="form-group">
                <label>TEMPAT LAHIR</label>
                <input type="text" class="form-control" name="tmpt_lhr" required>
              </div>
            </div>
			
			<div class="col-md-6">
              <div class="form-group">
                <label>NOMOR HP IBU</label>
                <input type="text" class="form-control" name="nmr_ibu" required>
              </div>
			  <div class="form-group">
                <label>FOTO SISWA</label>
                <input type="file" id="exampleInputFile" name="foto" required>
              </div>
            </div>
          </div>
		  
		  <BR>
          <div class="box-footer">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" name="form-input" class="btn btn-info pull-right">Submit</button>
          </div>
		 </form>
        </div>
        <!-- /.box-body -->
       
      </div>
      <!-- /.box -->

      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <?php include"layout/footer.php"; ?>