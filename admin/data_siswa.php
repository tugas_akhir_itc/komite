<?php 
	include"../inc/config.php";
	include"../inc/function.php"; 
	validate_admin_not_login("login.php");
	include"layout/header.php";
	
	if(!empty($_GET)){
		if($_GET['act'] == 'delete'){
			
			$q = mysql_query("delete from siswa WHERE id_siswa='$_GET[id]'");
			if($q){ alert("Success"); redir("data_siswa.php"); }
		}  
	}
?>
  <section class="content-header">
      <h1>
        Data Siswa
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="data_siswa.php">Data Siswa</a></li>
      </ol>
    </section>
   <!-- Main content -->
    <section class="content">
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
			<h3 class="box-title pull right"> <a class='btn btn-danger btn-xm' href='input_siswa.php'><span class='glyphicon glyphicon-plus'></span> Tambah Data Siswa</a></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
		
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
			<div class="col-md-12">
		 	 
			 <table id="example1" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>No</th>
								<th>Nisn</th>
								<th>Nama</th>
								<th>Kelas</th>
								<th>Tanggal Lahir</th>
								<th>Agama</th>
								<th>Alamat</th>
								<th>Aksi</th>
								
							</tr>
						</thead>
						<tbody>
							<?php
								$q = mysql_query("Select * from siswa") or die (mysql_error());
								while ($data = mysql_fetch_object($q)) {
									?>
									<tr>
										<td><?php echo $no++; ?></td>
										<td><?php echo $data->nisn; ?></td>
										<td><?php echo $data->nama; ?></td>
										<td><?php echo $data->kelas; ?></td>
										<td><?php echo $data->tgl_lahir; ?></td>
										<td><?php echo $data->agama; ?></td>
										<td><?php echo $data->alamat; ?></td>
										<td>
										<a class='btn btn-danger btn-xs' title='Lihat Detail' href='detail_siswa.php?act=delete&&id=<?php echo $data->id_siswa ?>'><span class='glyphicon glyphicon-search'></span></a>
										<a class='btn btn-info btn-xs' title='Edit Siswa' href='edit_siswa.php?id=<?php echo $data->id_siswa; ?>'><span class='glyphicon glyphicon-edit'></span></a>
										<a class='btn btn-danger btn-xs' title='Delete Siswa' href='data_siswa.php?act=delete&&id=<?php echo $data->id_siswa ?>' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\"><span class='glyphicon glyphicon-trash'></span></a>
										</td>
										
									</tr>
									<?php
								}
							?>
							
						</tbody>
					</table>
			 
        </div>
        <!-- /.box-body -->
       
      </div>
      <!-- /.box -->

      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <?php include"layout/footer.php"; ?>