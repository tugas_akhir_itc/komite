<?php 
	include"../inc/config.php";
	include"../inc/function.php"; 
	validate_admin_not_login("login.php");
	include"layout/header.php";
	
	if(!empty($_GET)){
		if(@$_GET['act'] == 'delete'){
			
			$q = mysql_query("delete from siswa WHERE id_siswa='$_GET[id]'");
			if($q){ alert("Success"); redir("data_siswa.php"); }
		}  
	}
?>
  <section class="content-header">
      <h1>
        Pembayaran Siswa
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Pembayaran Siswa</a></li>
      </ol>
    </section>
   <!-- Main content -->
    <section class="content">
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
			<div class="box-title pull right"> 
			<form class="form-inline pull-right"  method="GET">
				 
						<div class="form-group">
							<div class='input-group'>
								<input type='text' class="form-control" name="nisn" value="" placeholder="Masukkan Nim" maxlength="10" minlength="10" required/>
								
							</div>
						</div>
						
				  <button type="submit" name="cari" class="btn btn-danger"><span class="glyphicon glyphicon-search"></span> Cari</button>
				</form> 
			</div>
			<div class="box-title pull right" style="padding-left:100px"> 
			<form target='_BLANK' action="laporank.php" class="form-inline pull-right"  method="GET">
				 
						<div class="form-group">
							<div class='input-group'>
							<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
							</div>
								<input type='text' class="form-control" name="dari" value="" placeholder="Dari" id="datepicker" required/>
							</div>
						</div>
						<div class="form-group">
							<div class='input-group'>
							<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
							</div>
								<input type='text' class="form-control" name="sampai" value="" placeholder="Sampai" id="datepicker2" required/>
							</div>
						</div>
						
				  <button type="submit" name="print" class="btn btn-warning"><span class="glyphicon glyphicon-print"></span> Print Semua Pembayaran</button>
				</form> 
			</div>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
		
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
			<div class="col-md-12">
		 	 
			 <table id="example2" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>No</th>
								<th>Nisn</th>
								<th>Nama</th>
								<th>kelas</th>
								<th>Tanggal Lahir</th>
								<th>Agama</th>
								<th>Alamat</th>
								<th>Aksi</th>
								
							</tr>
						</thead>
						<tbody>
							<?php
								$q = mysql_query("select * from siswa where id_siswa='0'") or die (mysql_error());
								if(isset($_GET['cari'])){
								$nisn=$_GET['nisn'];
								$q = mysql_query("select * from siswa where nisn='$nisn'");
								}
								while ($data = mysql_fetch_object($q)) {
									?>
									
									<tr>
										<td><?php echo $no++; ?></td>
										<td><?php echo $data->nisn; ?></td>
										<td><?php echo $data->nama; ?></td>
										<td><?php echo $data->kelas; ?></td>
										<td><?php echo $data->tgl_lahir; ?></td>
										<td><?php echo $data->agama; ?></td>
										<td><?php echo $data->alamat; ?></td>
										<td>
										<a class='btn btn-default btn-xs' title='Tambah Pembayaran Siswa' href='t_pembayaran.php?id=<?php echo $data->id_siswa; ?>'><span class='glyphicon glyphicon-euro'></span></a>
										<a class='btn btn-info btn-xs' title='Detail Pembayaran Siswa' href='detail_pembayaran.php?id=<?php echo $data->id_siswa; ?>'><span class='glyphicon glyphicon-search'></span></a>
										<a class='btn btn-danger btn-xs' title='Print Pembayaran' target='_BLANK' href='laporan.php?id=<?php echo $data->id_siswa; ?>'><span class='glyphicon glyphicon-print'></span></a>
										</td>
										
									</tr>
									<?php
								}
							?>
							
						</tbody>
					</table>
			 
        </div>
        <!-- /.box-body -->
       
      </div>
      <!-- /.box -->

      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <?php include"layout/footer.php"; ?>