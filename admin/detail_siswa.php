<?php 
	include"../inc/config.php";
	include"../inc/function.php"; 
	validate_admin_not_login("login.php");
	include"layout/header.php";
	
	$query = "select * from siswa where id_siswa = '$_GET[id]'";
	$result = mysql_query($query);
	$data = mysql_fetch_object($result);
?>
  <section class="content-header">
      <h1>
        Detail Data Siswa
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="data_siswa.php">Data Siswa</a></li>
        <li class="active">Detail Siswa</li>
      </ol>
    </section>
   <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
			<h3 class="box-title"></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" onclick="history.back(-1)" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
		
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
				
		  	<img class='img-thumbnail' src="<?php echo '../image/'.$data->foto ?>"style='width:180px; height:200px' >
		  
			<div class="col-md-9">
			<table class="table table-bordered table-hover">
			 <tr>
                <td width="100px">NISN</td>
                <td width="200px"><?php echo $data->nisn?></td>
				<td width="100px">KELAS</td>
                <td width="200px"><?php echo $data->kelas?></td>
			</tr>
			<tr>
                <td>PASSWORD</td>
                <td>********</td>
				<td>TANGGAL LAHIR</td>
                <td><?php echo $data->tgl_lahir ?></td>
            </tr>
			<tr>
                <td>NAMA SISWA</td>
                <td><?php echo $data->nama ?></td>
				<td>JENIS KELAMIN</td>
                <td><?php echo $data->jenis_kelamin ?></td>
            </tr>
			<tr>
                <td>ALAMAT SISWA</td>
                <td><?php echo $data->alamat ?></td>
				<td>NAMA AYAH</td>
                <td><?php echo $data->nama_ayah ?></td>
            </tr>
			<tr>
                <td>RT/RW</td>
                <td><?php echo $data->rt_rw ?></td>
				<td>PEKERJAAN AYAH</td>
                <td><?php echo $data->pekerjaan_ayah ?></td>
            </tr>
			<tr>
                <td>KECAMATAN</td>
                <td><?php echo $data->kecamatan ?></td>
				<td>PENGHASILAN AYAH</td>
                <td><?php echo $data->penghasilan_ayah ?></td>
            </tr>
			<tr>
                <td>KELURAHAN</td>
                <td><?php echo $data->kelurahan ?></td>
				<td>NOMOR HP AYAH</td>
                <td><?php echo $data->nomor_hp_ayah ?></td>
            </tr>
			<tr>
                <td>KODE POS</td>
                <td><?php echo $data->kode_pos ?></td>
				<td>NAMA AYAH</td>
                <td><?php echo $data->nama_ayah ?></td>
            </tr>
			<tr>
                <td>NO HANDPHONE</td>
                <td><?php echo $data->telepon ?></td>
				<td>PENGHASILAN IBU</td>
                <td><?php echo $data->penghasilan_ibu ?></td>
            </tr>
			<tr>
                <td>AGAMA</td>
                <td><?php echo $data->agama ?></td>
				<td>PEKERJAAN IBU</td>
                <td><?php echo $data->pekerjaan_ibu ?></td>
            </tr>
			<tr>
                <td>EMAIL</td>
                <td><?php echo $data->email ?></td>
				<td>NOMOR HP IBU</td>
                <td><?php echo $data->nomor_hp_ibu ?></td>
            </tr>
			<tr>
				<td width="100px">TEMPAT LAHIR</td>
                <td width="200px"><?php echo $data->tmpt_lahir?></td>
			</tr>
			<tr>
                <td><button type="reset" onclick="history.back(-1)" class="btn btn-default">Back</button></td>
            </tr>
		 </table>
		 <table border="2">
		 </table>
        </div>
        <!-- /.box-body -->
       
      </div>
      <!-- /.box -->

      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <?php include"layout/footer.php"; ?>