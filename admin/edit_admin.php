<?php 
	include"../inc/config.php";
	include"../inc/function.php"; 
	validate_admin_not_login("login.php");
	include"layout/header.php";
	$query = "select * from admin where id = '$_GET[id]'";
	$result = mysql_query($query);
	$data = mysql_fetch_object($result);
?>
  <section class="content-header">
      <h1>
        Tambah Data Admin
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="data_admin.php" >Data Admin</a></li>
        <li class="active">Tambah Data Admin</li>
      </ol>
    </section>
   <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
			<h3 class="box-title"></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" onclick="history.back(-1)" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
		
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
		  
		  <?php 
			if(!empty($_POST)){
			$foto=$_FILES['foto']['name'];
			extract($_POST);
			$pass = md5($pass);
			$q = mysql_query("update admin set nama='$nama', tgl_lahir='$tgl_lhr', jk='$jk', alamat='$alamat', telepon='$tlp', foto='$foto', email='$email', password='$pass', status='$status' where id='$_GET[id]'");
			move_uploaded_file($_FILES['foto']['tmp_name'], "../image/".$foto);
				if($q){ 		
				echo "<script>alert('Data Berhasil Disimpan');document.location.href='data_admin.php'</script>\n";
					} else {
					echo "<script>alert('Terjadi kesalahan dalam pengisian form. Silahkan Coba Lagi');document.location.href='data_admin.php'</script>\n";
						}	
					}
				?>
		  
			<div class="col-md-6">
		 	 <form class="form" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label>EMAIL</label>
                <input type="email" class="form-control" name="email" value="<?php echo $data->email?>" required>
				<input type="hidden" class="form-control" name="status" value="<?php echo $data->status?>" required>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>PASSWORD</label>
                <input type="password" class="form-control" name="pass" placeholder="******" required>
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                <label>ALAMAT</label>
                <input type="text" class="form-control" name="alamat" value="<?php echo $data->alamat?>" required>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>TANGGAL LAHIR</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" name="tgl_lhr" id="datepicker" value="<?php echo $data->tgl_lahir?>" required>
                </div>
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
			<div class="col-md-6">
              <div class="form-group">
                <label>NAMA</label>
                <input type="text" class="form-control" name="nama" value="<?php echo $data->nama?>" required>
              </div>
              <div class="form-group">
                <label>TELEPON</label>
                <input type="text" class="form-control" name="tlp" value="<?php echo $data->telepon?>" required>
              </div>
            </div>
			
			<div class="col-md-6">
              <div class="form-group">
                <label>JENIS KELAMIN</label>
                <select class="form-control select2" name="jk" style="width: 100%;" required>
				<?php
						$jk=$data->jk;
						if($jk=='Laki - Laki'){
						?>
							<option value="Laki - Laki" selected="selected">Laki - Laki</option>
                			<option value="Perempuan" >Perempuan</option>
						<?php
						}
						else{
						?>
							<option value="Laki - Laki">Laki - Laki</option>
                			<option value="Perempuan" selected="selected">Perempuan</option>
								
						<?php
						}
				?>
                </select>
              </div>
              <div class="form-group">
                <label>FOTO</label>
                <input type="file" id="exampleInputFile" name="foto" required>
              </div>
            </div>
          </div>
		  
		  <BR>
          <div class="box-footer">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" name="form-edit" class="btn btn-info pull-right">Submit</button>
          </div>
		 </form>
        </div>
        <!-- /.box-body -->
       
      </div>
      <!-- /.box -->

      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <?php include"layout/footer.php"; ?>