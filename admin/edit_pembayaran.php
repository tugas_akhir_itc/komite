<?php 
	include"../inc/config.php";
	include"../inc/function.php"; 
	validate_admin_not_login("login.php");
	include"layout/header.php";

	@$p = mysql_fetch_object(mysql_query("select * from pembayaran where id_pembayaran=$_GET[id]"));
	@$q = mysql_query("select * from siswa inner join pembayaran on siswa.id_siswa=pembayaran.id_siswa where id_pembayaran='$p->id_pembayaran'") or die (mysql_error());
	$data = mysql_fetch_object($q)
?>
  <section class="content-header">
      <h1>
        Edit Pembayaran Siswa
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="data_siswa.php">Pembayaran Siswa</a></li>
		<li><a href="detail_pembayaran.php?id=<?php echo $data->id_siswa; ?>">Detail Pembayaran Siswa</a></li>
        <li class="active">Edit Pembayaran Siswa</li>
      </ol>
    </section>
   <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
			<h3 class="box-title"></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
		
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
		  
		   
			<?php 
			if(!empty($_POST)){
			extract($_POST);
			$date=date('Y-m-d');
			$time=date('h:i:s');
			$q = mysql_query("update pembayaran SET id_siswa='$p->id_siswa', jenis_pembayaran='KOMITE', semester='$s', jml_pembayaran='$jml', tanggal='$date', time='$time' where id_pembayaran='$data->id_pembayaran'");
				if($q){  
				
				echo "<script>alert('Pembayaran Berhasil Disimpan');document.location.href='edit_pembayaran.php?id=$data->id_pembayaran'</script>\n";
					} else {
					echo "<script>alert('Terjadi kesalahan dalam pengisian form. Silahkan Coba Lagi');document.location.href='edit_pembayaran.php?id=$data->id_pembayaran'</script>\n";
						}	
					}
				?>
				
			<div class="col-md-6">
			
		 	 <form class="form" method="post" enctype="multipart/form-data" action="">
              <div class="form-group">
                <label>NISN</label>
                <input type="text" class="form-control" name="nisn" value="<?php echo $data->nisn?>" readonly="readonly">
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>NAMA</label>
                <input type="text" class="form-control" name="nama" value="<?php echo $data->nama?>" readonly="readonly">
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                <label>SEMESTER</label>
                <input type="text" class="form-control" name="s" value="<?php echo $data->semester?>" required>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>JUMLAH PEMBAYARAN</label>
                <input type="text" class="form-control" name="jml" value="<?php echo $data->jml_pembayaran?>" required>
              </div>
              <!-- /.form-group -->
            </div>
			
          </div>
		  
		  <BR>
          <div class="box-footer">
		  <button type="reset" onclick="history.back(-1)" class="btn btn-danger pull-left">Back</button>
                <button type="submit" name="form-edit" class="btn btn-info pull-right">Submit</button>
          </div>
		 </form>
		 
        </div>
        <!-- /.box-body -->
       
      </div>
      <!-- /.box -->

      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <?php include"layout/footer.php"; ?>